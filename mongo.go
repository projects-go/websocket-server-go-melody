package main

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/olahol/melody.v1"
)

func main() {
	e := echo.New()
	m := melody.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/ws", func(c echo.Context) error {
		m.HandleRequest(c.Response(), c.Request())
		return nil
	})

	m.HandleMessage(func(s *melody.Session, msg []byte) {
		fmt.Printf("%s\n", msg)
		m.Broadcast(msg)
	})

	m.HandleClose(func(s *melody.Session, i int, msg string) error {
		fmt.Printf("%s\n", msg)
		m.Close();
		return nil
	})

	e.Logger.Fatal(e.Start(":8080"))
}